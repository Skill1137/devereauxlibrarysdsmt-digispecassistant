#include <iostream> 
#include <fstream> 
#include <string> 
#include <cstdlib>
#include <stdio.h> 
#include <algorithm> 

using namespace std;

//Name;Extension;Count;Last write date;Size;MD5;Attributes;Width;Height;BPP;Files count

struct file
{
	string 	name; 
	string 	ext; 
	string	writedate;
	int	size;
	string	MD5; 
	string	atrib;
	int	width;
	int	height;
	int	bpp; 
}; 

file createFile( ifstream &fin, int size );
void printFile ( ofstream &fout, file f );

int main()
{

	ifstream fin; 
	ofstream fout; 

	// get file
	cout << "Enter a filename: "; 
	string filename;  	
	string newname; 

	cin >> filename; 
	newname = filename + ".orig"; 

	// rename original
//	cout << "Creating Backup ... \n"; 
//	rename( filename.c_str(), newname.c_str()); 

	// try to open file
	fin.open(filename.c_str(), ios::in ); 
	if ( fin.is_open() )
		cout << "Opening " << filename << " ...\n"; 
	else
	{
		cout << "Could not open file\n";
		return 1; 
	}

	// create new file
	cout << "Creating New File ...\n"; 
	fout.open("a.out", ios::out); 

	// print necessary files
	file file; 
	while ( !fin.eof() )
	{
		file = createFile( fin, 250 ); 
		if ( file.ext == "tif" )
			printFile(fout, file);	
	}

	return 0; 
} 

file createFile( ifstream &fin, int size )
{
	file file; 
	char temp[250]; 

	// remove leading newline
	fin.getline( temp, 250 ); 

	// file name
	fin.getline( temp, 250, ';' ); 
	file.name = temp; 
	
	// extension
	fin.getline( temp, 250, ';' ); 
	file.ext = temp; 

	// break if not tiff file
	if ( file.ext != "tif" )
		return file; 

	// remove double ';'
	fin.getline( temp, 250, ';' ); 

	// last write date
	fin.getline( temp, 250, ';' ); 
	file.writedate = temp; 

	// file size
	fin.getline( temp, 250, ';' ); 
	file.size = atoi(temp); 

	// MD5 Value
	fin.getline( temp, 250, ';' ); 
	file.MD5 = temp; 

	// Attributes
	fin.getline( temp, 250, ';' ); 
	file.atrib = temp; 

	// Width
	fin.getline( temp, 250, ';' ); 
	file.width = atoi(temp); 

	// Height
	fin.getline( temp, 250, ';' ); 
	file.height = atoi(temp); 

	// BPP Value
	fin.getline( temp, 250, ';' ); 
	file.bpp = atoi(temp); 

	return file; 
}

void printFile ( ofstream &fout, file f )
{
	fout << f.name << ", "  << f.ext << ", " << f.name << '.' << f.ext << ", " << f.writedate << ", " << f.size << ", "
		<< f.MD5 << ", " << f.atrib << ", " << f.width << ", " << f.height << ", " << f.bpp << '\n';  
}
